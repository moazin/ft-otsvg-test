#include <cairo.h>
#include <librsvg/rsvg.h>
#include <ft2build.h>
#include FT_FREETYPE_H

  typedef struct Rsvg_Port_StateRec_
  {
    cairo_surface_t    *rec_surface;
    double             x;
    double             y;
  } Rsvg_Port_StateRec;

  typedef struct Rsvg_Port_StateRec_*  Rsvg_Port_State;

  FT_Error
  rsvg_port_init( FT_Library  library );

  void
  rsvg_port_free( FT_Library  library  );

  FT_Error
  rsvg_port_render( FT_GlyphSlot slot );

  FT_Error
  rsvg_port_preset_slot( FT_GlyphSlot  slot, FT_Bool  cache );
