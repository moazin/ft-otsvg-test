prefix=`pkg-config librsvg-2.0 --variable=libdir`
export LD_LIBRARY_PATH=$prefix
echo `ldd ./build/main | grep free`
echo `ldd ./build/main | grep rsvg`
./build/main ./fonts/gilbert.otf c g /images/gilbert-g.png
./build/main ./fonts/gilbert.otf c x /images/gilbert-x.png
./build/main ./fonts/EmojiOneColor.otf i 100 /images/emoji-100.png
./build/main ./fonts/TestSVGgzip.otf i 3 /images/gzip.png
./build/main ./fonts/TestSVGmultiGlyphs.otf i 9 /images/multi-light.png
./build/main ./fonts/TestSVGmultiGlyphs.otf i 13 /images/multi-dark.png
