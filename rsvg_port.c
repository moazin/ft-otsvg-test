#include <cairo.h>
#include <librsvg/rsvg.h>
#include <rsvg_port.h>
#include <ft2build.h>
#include <stdlib.h>
#include <math.h>
#include FT_FREETYPE_H
#include FT_INTERNAL_DEBUG_H
#include FT_INTERNAL_TRUETYPE_TYPES_H
#include FT_BBOX_H
#include FT_OTSVG_H

  FT_Error
  rsvg_port_init( FT_Library  library )
  {
    library->svg_renderer_state = malloc( sizeof( Rsvg_Port_StateRec ) );
    return FT_Err_Ok;
  }

  void
  rsvg_port_free( FT_Library  library )
  {
    free( library->svg_renderer_state );
  }

  FT_Error
  rsvg_port_render( FT_GlyphSlot slot )
  {
    FT_Error         error = FT_Err_Ok;

    Rsvg_Port_State    state;
    cairo_status_t     status;
    cairo_t            *cr;
    cairo_surface_t    *surface;

    state    = slot->library->svg_renderer_state;

    surface = cairo_image_surface_create_for_data( slot->bitmap.buffer,
                                                   CAIRO_FORMAT_ARGB32,
                                                   slot->bitmap.width,
                                                   slot->bitmap.rows,
                                                   slot->bitmap.pitch );
    status = cairo_surface_status( surface );

    if ( status != CAIRO_STATUS_SUCCESS )
    {
      if ( status == CAIRO_STATUS_NO_MEMORY )
        return FT_Err_Out_Of_Memory;
      else
        return FT_Err_Invalid_Outline;
    }

    cr     = cairo_create( surface );
    status = cairo_status( cr );

    if ( status != CAIRO_STATUS_SUCCESS )
    {
      if ( status == CAIRO_STATUS_NO_MEMORY )
        return FT_Err_Out_Of_Memory;
      else
        return FT_Err_Invalid_Outline;
    }

    cairo_translate( cr, -1 * (state->x), -1 * (state->y) );
    cairo_set_source_surface( cr, state->rec_surface, 0.0, 0.0 );
    cairo_paint( cr );

    cairo_surface_flush( surface );

    slot->bitmap.pixel_mode = FT_PIXEL_MODE_BGRA;
    slot->bitmap.num_grays  = 256;
    slot->internal->flags |= FT_GLYPH_OWN_BITMAP;
    slot->format = FT_GLYPH_FORMAT_BITMAP;

  CleanCairo:
    cairo_surface_destroy( surface );
    cairo_destroy( cr );
    cairo_surface_destroy( state->rec_surface );
    return error;
  }

  FT_Error
  rsvg_port_preset_slot( FT_GlyphSlot  slot, FT_Bool  cache )
  {
    /* FreeType variables */
    FT_Error         error          = FT_Err_Ok;
    FT_SVG_Document  document       = (FT_SVG_Document)slot->other;
    FT_Size_Metrics  metrics        = document->metrics;
    FT_UShort        units_per_EM   = document->units_per_EM;
    FT_UShort        end_glyph_id   = document->end_glyph_id;
    FT_UShort        start_glyph_id = document->start_glyph_id;

    /* Librsvg variables */
    GError             *gerror = NULL;
    gboolean           ret;
    gboolean           out_has_width;
    gboolean           out_has_height;
    gboolean           out_has_viewbox;
    RsvgHandle         *handle;
    RsvgLength         out_width;
    RsvgLength         out_height;
    RsvgRectangle      out_viewbox;
    RsvgPositionData   position_svg;
    RsvgDimensionData  dimension_svg;
    cairo_t            *rec_cr;
    cairo_matrix_t     transform_matrix;

    /* Rendering port's state */
    Rsvg_Port_State     state;
    Rsvg_Port_StateRec  state_dummy;

    /* generic variables */
    double  x;
    double  y;
    double  xx;
    double  xy;
    double  yx;
    double  yy;
    double  x0;
    double  y0;
    double  width;
    double  height;
    double  x_svg_to_out;
    double  y_svg_to_out;


    if ( cache )
      state = slot->library->svg_renderer_state;
    else
      state = &state_dummy;

    handle = rsvg_handle_new_from_data( document->svg_document,
                                        document->svg_document_length,
                                        &gerror );
    if ( handle == NULL )
    {
      error = FT_Err_Invalid_SVG_Document;
      goto CleanLibrsvg;
    }

    rsvg_handle_get_intrinsic_dimensions( handle,
                                          &out_has_width,
                                          &out_width,
                                          &out_has_height,
                                          &out_height,
                                          &out_has_viewbox,
                                          &out_viewbox );

    if ( out_has_viewbox == TRUE )
    {
      position_svg.x       = out_viewbox.x;
      position_svg.y       = out_viewbox.y;
      dimension_svg.width  = out_viewbox.width;
      dimension_svg.height = out_viewbox.height;
    }
    else if ( ( out_has_width == TRUE ) && ( out_has_height == TRUE ) )
    {
      position_svg.x       = 0;
      position_svg.y       = 0;
      dimension_svg.width  = out_width.length;
      dimension_svg.height = out_height.length;
    }
    else
    {
      position_svg.x       = 0;
      position_svg.y       = 0;
      dimension_svg.width  = units_per_EM;
      dimension_svg.height = units_per_EM;
    }

    x_svg_to_out = (float)metrics.x_ppem/(float)dimension_svg.width;
    y_svg_to_out = (float)metrics.y_ppem/(float)dimension_svg.height;

    state->rec_surface = cairo_recording_surface_create( CAIRO_CONTENT_COLOR_ALPHA,
                                                         NULL );

    rec_cr = cairo_create( state->rec_surface );


    /* create the transformation */
    xx =  1 * (((double)document->transform.xx)/((double)(1 << 16 )));
    xy = -1 * (((double)document->transform.xy)/((double)(1 << 16 )));
    yx = -1 * (((double)document->transform.yx)/((double)(1 << 16 )));
    yy =  1 * (((double)document->transform.yy)/((double)(1 << 16 )));
    x0 =  1 * ((double)document->delta.x/(double)(1 << 6)) *
              ((double)dimension_svg.width/(double)metrics.x_ppem);
    y0 = -1 * ((double)document->delta.y/(double)(1 << 6)) *
              ((double)dimension_svg.height/(double)metrics.y_ppem);

    transform_matrix = (cairo_matrix_t){ xx, yx, xy, yy, x0, y0 };
    cairo_scale( rec_cr, x_svg_to_out, y_svg_to_out );
    cairo_transform( rec_cr, &transform_matrix );

    if ( start_glyph_id == end_glyph_id )
    {
      ret = rsvg_handle_render_cairo ( handle, rec_cr );
      if ( ret == FALSE )
      {
        error = FT_Err_Invalid_SVG_Document;
        goto CleanCairo;
      }
    }
    else if ( start_glyph_id < end_glyph_id )
    {
      /* The document contains multiple glyphs. Let's use the ID to render
       * our specific one.
       */
      int    length;
      char*  str;

      length = snprintf( NULL, 0, "%u", slot->glyph_index );
      str    = malloc( 6 + length + 1 );
      strcpy( str, "#glyph");
      snprintf( str + 6, length + 1, "%u", slot->glyph_index );
      ret = rsvg_handle_render_cairo_sub( handle, rec_cr, str );
      free(str);
      if ( ret == FALSE )
      {
        error = FT_Err_Invalid_SVG_Document;
        goto CleanCairo;
      }
    }

    cairo_recording_surface_ink_extents( state->rec_surface, &x, &y,
                                         &width, &height);

    state->x            = x;
    state->y            = y;

    slot->bitmap_left   = state->x;
    slot->bitmap_top    = state->y * -1;
    slot->bitmap.rows   = round( height );
    slot->bitmap.width  = round( width );
    slot->bitmap.pitch  = slot->bitmap.width * 4;

    slot->bitmap.pixel_mode = FT_PIXEL_MODE_BGRA;

    if ( cache == TRUE )
    {
      cairo_destroy( rec_cr );
      goto CleanLibrsvg;
    }

  CleanCairo:
    cairo_surface_destroy( state->rec_surface );
    cairo_destroy( rec_cr );
  CleanLibrsvg:
    g_object_unref( handle );
    return error;
  }
