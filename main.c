#include <ft2build.h>
#include <cairo.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OTSVG_H
#include FT_MODULE_H

 int main(int argc, char* argv[])
 {
    if(argc != 5)
    {
      printf("invalid args\n");
      return -1;
    }
    FT_Library library;
    FT_Error error = FT_Err_Ok;
    error = FT_Init_FreeType(&library);
    if(error){
        printf("Error initializing the library");
        return 0;
    }
    FT_Face face;
    error = FT_New_Face(library, argv[1], 0, &face);
    if(error){
        printf("Error loading the font");
        return 0;
    }
    error = FT_Set_Pixel_Sizes(face, 1000, 1000);
    if(error){
        printf("Error setting the char size");
        return 0;
    }

    printf("--------------------\n");
    printf("font:        %s\n", argv[1]);
    unsigned int glyph_index;
    if ( argv[2][0] == 'c' )
    {
      glyph_index = FT_Get_Char_Index( face, argv[3][0] );
      printf("character:   %c\n", argv[3][0]);
      printf("index:       %d\n", glyph_index);
    }
    else if ( argv[2][0] == 'i' )
    {
      sscanf( argv[3], "%d", &glyph_index );
      printf("index:       %d\n", glyph_index);
    }

    FT_Glyph  glyph_color;
    error = FT_Load_Glyph(face, glyph_index, FT_LOAD_COLOR);
    if(error){
        printf("Error loading the glyph");
        return 0;
    }
    FT_Get_Glyph(face->glyph, &glyph_color);

    /* transforms */
    FT_Glyph_To_Bitmap(&glyph_color, FT_RENDER_MODE_NORMAL, NULL, 0);

    FT_BitmapGlyph bit_glyph_color = (FT_BitmapGlyph)glyph_color;

    cairo_surface_t *surf = cairo_image_surface_create_for_data( bit_glyph_color->bitmap.buffer,
                                                                 CAIRO_FORMAT_ARGB32,
                                                                 bit_glyph_color->bitmap.width,
                                                                 bit_glyph_color->bitmap.rows,
                                                                 bit_glyph_color->bitmap.pitch );
    cairo_surface_write_to_png( surf, argv[4] );

    printf("width:       %d\n", bit_glyph_color->bitmap.width);
    printf("height:      %d\n", bit_glyph_color->bitmap.rows);
    printf("pitch:       %d\n", bit_glyph_color->bitmap.pitch);
    printf("bitmap_left: %d\n", bit_glyph_color->left);
    printf("bitmap_top:  %d\n", bit_glyph_color->top);
    printf("advance_x:   %ld\n", bit_glyph_color->root.advance.x);
    printf("advance_y:   %ld\n", bit_glyph_color->root.advance.y);

    FT_Done_Face( face );
    FT_Done_FreeType( library );
   return 0;
 }
