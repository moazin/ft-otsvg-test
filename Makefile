current_dir_include = -I./
freetype_include = -I../freetype2/include
port_include = -I../port
cairo_include = $(shell pkg-config cairo --cflags | sed "s/\/freetype2//g")

linker_cairo = $(shell pkg-config cairo --libs | sed "s/-lfreetype//g")

includes = $(freetype_include) $(cairo_include)
pwd = $(shell pwd)
freetype_path = $(pwd)/../freetype2/objs/.libs
rsvg_path = $(pwd)/../librsvg/.libs/
linker_flags = $(linker_cairo) \
							 -Wl,-rpath $(rsvg_path) -Wl,-rpath $(freetype_path)
all:
	gcc -g -O0 -c $(includes) main.c -o build/main.o
	gcc -g -O0 build/main.o -lfreetype -o build/main $(linker_flags)
